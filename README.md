# R-and-Rstudio-self-documentation
Documentation of R and Rstudio functions to be used as a reference guide of 
general functions. 

Please download the files if you would like to view the contents as the pdf is
not viewing properly with gitlab and the Rmd file also displays issues in gitlab.
Note that I tend to update the contents of these files, so check back regularly
to see if new content is added. If you're not familiar with gitlab (I'm still 
learning as well), this page you are on right now will show the latest documents.
To see any changes, go to the left hand side of the screen, select repository and
then select commits. Here you will see any new chanegs made to the documents.

Feel free to download and open the Rmd file (which is a markdown file). This file
is used to create the pdf from within R. Everything you see in the pdf is also in
the rmd file.

There are disclaimers to this document. 

I started learning R a little over a year ago. I created this document initially 
for myself, which I thought would be beneficial for me as a reference guide. 
I am not an experienced scripter and my memory is not the best ever, so I
generally require an aid to look up how I did certain things in my work. I
noted however that others may have some use out of this like I did. I know that
when I started learning R I would have loved a document like this to help me
on my way, instead of figuring it out myself using many online searches (and
an incredible amount of frustration and cursing). 

Because I did this initially for myself, I could not be bothered by citing all
the solutions to my R problems that I could find on StackOverflow, hence the
lack of citations. 

As said before, I am not an experienced scripter. Could more advanced R users
think of better more efficient ways to get things done... probably most likely.
Could more advanced users find prettier ways of coding and using Rmarkdown....
probably most likely. I know that there is a thing such as Rprojects, but I have
not gotten around getting really familiar with it (which I probably should do). 
I know there is Shiny apps, but I have not gotten around getting really familiar 
with it (which I probably should do). This is in no way an official guide and I
am not liable for any 'damages' resulting from the use of these functions. It has 
been created with the best intentions. I keep learning and finding new things in 
R, I keep improving and learn how to do things better. Do not ever let 'advanced'
users bully you into thinking you're idiotic for doing things the way you do while
learning. (hopefully you will have advanced users around that will have the 
patience in teaching you (like I luckily had) without making you feel bad)

Yes, I know that you can look up or search online on how to do things in R, but
if you don't know where to start, it can be quite difficult to find what you need.
In addition, I don't always find the help pages of R really intuitive.

If there are things you don't like or are stupid in your eyes, you don't have to
use this document. But I hope that someone may find some things that helped them
in learning R. 

Repeating myself again: I created this for myself. Things might seem cryptic to you
but are familiar to me as I had to use it in my own work while learning R from the 
beginning.

The Rmd document has been written using Rmarkdown with knitting to pdf. You 
will need LaTex to knit to pdf. If you don't have this, you can install the R 
package TinyTex. 

